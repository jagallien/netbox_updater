#!/usr/bin/python3

'''
   Netbox Update Script
   J.A. Gallien
   Used to automatically backup & update the Netbox server.
   Supposed to e-mail admins when done
'''


from datetime import date
import subprocess
import datetime
import smtplib
import socket
import os



todays_date = str(date.today().strftime('%Y%m%d'))
hostname = socket.gethostname()

logfile = open((todays_date + '_' + hostname[:14] + '_log.txt'), mode='w') # The log. We write all output of the commands to this file.

# These commands are for running the update

## TODO: create system-account w/ only permissions for yum,git,etc... remove individual update statements and use upgrade.sh instead
cmds = ['cd /opt/netbox/netbox/',
    'yum update -y',
    'git checkout master',
    'git pull origin master',
    'git status',
    'find . -name \"*.pyc\" -delete',
    'pip3 uninstall -r old_requirements.txt -y',
    'pip3 install -r requirements.txt --upgrade',
    'python3 netbox/manage.py migrate',
    'python3 netbox/manage.py collectstatic --no-input',
    'supervisorctl restart netbox'
]




# These commands are done to create a backup of existing data... in the event of an update causing the system to go belly-up.

dumpfile = os.system('pg_dump -U netbox -h localhost -w -f ' + todays_date + '_' + hostname[:14] + '_psqldump.txt')

for cmd in cmds:
    result = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = (result.communicate())
    output = str(output, encoding='UTF-8')
    logfile.write(output)


logfile.close()
message = open((todays_date + '_' + hostname[:14] + '_log.txt'), mode='r')

def emailer():
   global todays_date
   smtp_obj = smtplib.SMTP('<e-mail_server', 25)
   smtp_obj.ehlo()
   smtp_obj.sendmail('to', 'from', ('Subject:' + todays_date + ' Netbox Update\n' + message.read()))

emailer()

os.system('mv ' + todays_date + '_' + hostname[:14] + '_psqldump.txt where_to_dump_file')
